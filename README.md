**Sub Telegram BOT**<br/>
*A Bash Telegram BOT *<br/>
Created by pier_subgenius<br/>
[https://gitlab.com/piersubgenius](https://gitlab.com/piersubgenius)<br/>

Dependencies: curl, sqlite3, uuid, base64, jq<br/>

Type subtbot without commands for help.<br/>

**Commands:**
```
config			Configuration commands
export			Export commands
import			Import commands
license			Show SUBTBOT license
log			Show log
log last [LINES]	Show last few lines of log (default 10)
lsdata			List backed up data
lsdata last [LINES]	List backed up data last few lines (default 10)
start			Start BOT
stop			Stop BOT
status			Show SubTBOT status
```
**Config commands:**
```
config show				Show current configuration
config [ITEM] [PARAMETER]		Configuration sintax
config telegramtoken			Configure Telegram token
config telegramchatid			Configure Telegram chat id
config msgsleep				Configure message check time
config lastmasg				Configure last message id
config botcommand			Add or update botcommand
config no botcommand [BOTCOMMAND]	Remove a botcommand
```
***config botcommand sintax:***
```
Sintax: config botcommand [BOTCOMMAND] command [COMMAND] description [DESCRIPTION]
(use __ character for space in command and description)
```
**Config show example:**
```
current configuration:
!
!! subtbot version 1.1.*
!! subtbotdb /var/lib/subtbot.db
!! subtbotdbpath /var/lib
!! subtbot database version 1.1.6
!
subtbot config msgsleep 2
subtbot config lastmsg 0000
subtbot config telegramtoken 1234567890:AABBCCDDEEFFGGHHII
subtbot config telegramchatid 1234567890
!
subtbot config botcommand /ups command apcaccess description Show__UPS__status
!
subtbot config end
```
**Export commands:**
```
export config [CONFIGFILE]	Export configuration to file
export log [CSVFILE]		Export all logs to file in CSV format
export data [UUID] terminal	Export data for a specific uuid to terminal
export data [UUID] [FILE]	Export data for a specific uuid
```
**Import commands:**
```
import config [CONFIGFILE]	Import configuration from file
```
